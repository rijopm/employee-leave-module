<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->text('password')->nullable();
            $table->string('is_active')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        $employees = array([
            'name' => 'Rijo Chandy P M',
            'email' => 'rijo@hulkapps.in',
            'password' => bcrypt('password'),
          ],
          [
            'name' => 'Abin Thomas',
            'email' => 'abin@hulkapps.in',
            'password' => bcrypt('password'),
          ],
          [
            'name' => 'Rajeev Kumar',
            'email' => 'rajeev@hulkapps.in',
            'password' => bcrypt('password'),
          ],
        );

        DB::table('employees')->insert($employees);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
