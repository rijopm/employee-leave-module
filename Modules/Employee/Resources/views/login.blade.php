@extends('employee::layouts.master')
@section('content')
<div class="auth-layout-wrap" style="background-image: url({{asset('public/assets/images/photo-wide-4.jpg')}})">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="p-4">
                        <div class="auth-logo text-center mb-4"><h3>Employee</h3></div>
                        <h1 class="mb-3 text-18">Sign In</h1>
                        <form id="form" method="post" class="needs-validation" novalidate="novalidate">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control form-control-rounded" id="email" type="email" required="required" name="email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control form-control-rounded" id="password" type="password" name="password" required="required">
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2 ladda-button" data-style="expand-right">Sign In</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
		$('#form').validate({
	        submitHandler: function(form) {
	            var formData = new FormData(form);
	            var l = Ladda.create(document.querySelector('.ladda-button'));
	            l.start();
	            $.ajax({
	                url: base_url+'/employee/login',
	                headers: {'X-CSRF-TOKEN': csrf_token},
	                dataType: 'json',
	                data:formData,
	                contentType: false,
	                cache: false,
	                processData: false,
	                type: 'POST',
	                success: function (resp) {
	                    console.log(resp);
	                    if(resp.errorcode =='0')
	                    {
	                        toastr.success(resp.message, 'Success');
	                        l.stop();
	                        location.href = base_url+'/employee/leave';
	                    }
	                    else if (resp.errorcode == '1')
	                    {
	                        var html = '';
	                        $.each(resp.message, function(i,val){
	                            html+= val+'</br>';
	                        });
	                        toastr.error(html,'Error');
	                        l.stop();
	                    }
	                    else if (resp.errorcode == '2')
	                    {
	                        toastr.error(resp.message,'Error');
	                        l.stop();
	                    }
	                }
	            });
	        }
	    });
	</script>
@endsection
