<!DOCTYPE html>
<html lang="en" dir="">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Employee Leave Module</title>
        <link rel="shortcut icon" href="{{asset('public/home-assets/images/colins-fav.png')}}">
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
        <link href="{{asset('public/assets/css/themes/lite-purple.min.css')}}" rel="stylesheet" />
        <link href="{{asset('public/assets/css/plugins/perfect-scrollbar.min.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('public/assets/css/plugins/ladda-themeless.min.css')}}" />
        <link rel="stylesheet" href="{{asset('public/assets/css/plugins/toastr.css')}}" />
        <link rel="stylesheet" href="{{asset('public/assets/css/plugins/datatables.min.css')}}" />
        <link rel="stylesheet" href="{{asset('public/assets/css/plugins/sweetalert2.min.css')}}" />
        <link rel="stylesheet" href="{{asset('public/assets/choices.js/choices.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/summernote/summernote-lite.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
        <!-- <link rel="stylesheet" href="{{asset('public/assets/css/plugins/bootstrap/bootstrap.min.css')}}" /> -->
        <link rel="stylesheet" href="{{asset('public/assets/css/plugins/cropper.css')}}" />

    </head>

    <body class="text-left">

        @yield('content')
        <!-- ============ Search UI End ============= -->
        <script src="{{asset('public/assets/js/plugins/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/perfect-scrollbar.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/sidebar.large.script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/echarts.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/echart.options.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/dashboard.v1.script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/customizer.script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/jquery.validate.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/spin.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/ladda.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/ladda.script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/toastr.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/toastr.script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/datatables.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/datatables.script.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/sweetalert2.min.js')}}"></script>
        <script src="{{asset('public/assets/js/scripts/sweetalert.script.min.js')}}"></script>
        <script src="{{asset('public/assets/choices.js/choices.min.js')}}"></script>
        <script src="{{asset('public/assets/summernote/summernote-lite.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/cropper.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/popper.min.js')}}"></script>
        <script src="{{asset('public/assets/js/plugins/moment.js')}}"></script>
        <script type="text/javascript">
            var base_url="{{url('/')}}";
            var storage_url="{{storage_url()}}";
            var csrf_token="{{csrf_token()}}";
        </script>


        @yield('script')
    </body>

</html>
