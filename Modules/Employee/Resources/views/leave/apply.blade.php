@extends('employee::layouts.master')
@section('content')
	@include('employee::layouts.header')
	@include('employee::layouts.sidebar')
  <div class="main-content">
        <div class="breadcrumb">
            <h1>Leave</h1>
            <ul>
                <li><a href="{{url('employee/leave')}}">Leave</a></li>
                <li>Apply</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                  <form id="form" method="post" class="needs-validation" novalidate="novalidate">
                      <div class="card-body">
                          <div class="card-title mb-3">Leave Details</div>
                          <div class="row">
															<div class="col-md-6 form-group mb-3">
																	<label>Leave Type*</label>
																	<select class="form-control" required="required" name="leave_type">
																			<option value="cl">Casual Leave</option>
																			<option value="sl">Sick Leave</option>
																			<option value="pl">Paid Leave</option>
																			<option value="lwp">Leave Without Pay</option>
																	</select>
															</div>
															<div class="col-md-6 form-group mb-3 mt-4">
																<label class="checkbox checkbox-primary">
																		<input type="checkbox" name="is_half"><span>Is Half Day ?</span><span class="checkmark"></span>
																</label>
															</div>
															<div class="col-md-6 form-group mb-3">
																	<label>From Date*</label>
																	<input class="form-control" type="date" placeholder="Enter Date" required="required" name="from_date" />
															</div>
															<div class="col-md-6 form-group mb-3">
																	<label>To Date*</label>
																	<input class="form-control" type="date" placeholder="Enter Date" required="required" name="to_date" />
															</div>
                              <div class="col-md-6 form-group mb-3">
                              	<label>Reason*</label>
                              	<textarea class="form-control" name="reason" required></textarea>
                              </div>
															<div class="col-md-12 mt-5" align="right">
                                  <button class="btn btn-primary ladda-button" data-style="expand-right" type="submit">Submit</button>
                                  <a href="{{url('employee/leave')}}" class="btn btn-secondary">Cancel</a>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
	@include('employee::layouts.footer')
@endsection

@section('script')
	<script type="text/javascript">
    $('#form').validate({
	        submitHandler: function(form) {
	            var formData = new FormData(form);
	            var l = Ladda.create(document.querySelector('.ladda-button'));
	            l.start();
	            $.ajax({
	                url: base_url+'/employee/leave/store',
	                headers: {'X-CSRF-TOKEN': csrf_token},
	                dataType: 'json',
	                data:formData,
	                contentType: false,
	                cache: false,
	                processData: false,
	                type: 'POST',
	                success: function (resp) {
	                    console.log(resp);
	                    if(resp.errorcode =='0')
	                    {
	                        toastr.success(resp.message, 'Success');
	                        l.stop();
	                        location.href = base_url+'/employee/leave';
	                    }
	                    else if (resp.errorcode == '1')
	                    {
	                        var html = '';
	                        $.each(resp.message, function(i,val){
	                            html+= val+'</br>';
	                        });
	                        toastr.error(html,'Error');
	                        l.stop();
	                    }
	                    else if (resp.errorcode == '2')
	                    {
	                        toastr.error(resp.message,'Error');
	                        l.stop();
	                    }
	                }
	            });
	        }
	    });
	</script>
@endsection
