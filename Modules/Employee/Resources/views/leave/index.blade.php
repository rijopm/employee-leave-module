@extends('employee::layouts.master')
@section('content')
	@include('employee::layouts.header')
	@include('employee::layouts.sidebar')
  <div class="main-content">
		<div class="row">
			<div class="col-6">
		        <div class="breadcrumb">
		            <h1>Leave</h1>
		        </div>
		    </div>
	        <div class="col-6 text-right text-white">
	        	<a href="{{url('employee/leave/apply')}}" class="btn btn-raised ripple btn-raised-primary m-1">Apply Leave</a>
	        </div>
	    </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row mb-4">
            <div class="col-md-12 mb-4">
                <div class="card text-left">
                    <div class="card-body">
												<div class="row">
													<div class="col-md-3">
                        		<h4 class="card-title mb-3">Leave List</h4>
													</div>
													<div class="col-md-3">
														<p>Casual Leave Balance : {{$cl_balance}}</p>
													</div>
													<div class="col-md-3">
														<p>Paid Leave Balance : {{$pl_balance}}</p>
													</div>
													<div class="col-md-3">
														<p>Sick Leave Balance : {{$sl_balance}}</p>
													</div>
												</div>
                        <div class="table-responsive">
                            <table class="display table table-striped table-bordered" id="leave_table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Leave Type</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Reason</th>
                                        <th>Reply</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	@include('employee::layouts.footer')
@endsection

@section('script')
	<script type="text/javascript">
  $(function()
    {
        if($('#leave_table').length > 0)
        {
            $('#leave_table').DataTable({
                "bLengthChange": false,
                "bInfo" : false,
                "lengthMenu": [[25]],
                "paging": true,
                processing: true,
                serverSide: true,
                ajax: base_url+'/employee/leave/list',
                columns:
                [
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full,meta)
                        {
                            return meta.settings._iDisplayStart + meta.row + 1;
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.leave_type)
                           {
                             if(full.leave_type == 'cl')
                                return 'Casual Leave';
                             else if(full.leave_type == 'cl')
                                return 'Sick Leave';
                             else if(full.leave_type == 'pl')
                                return 'Paid Leave';
                             else if(full.leave_type == 'lwp')
                                return 'Leave Without Pay';
                             else
                                return 'NIL';
                           }
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.from_date)
                               return moment(full.from_date).format('DD-MM-YYYY');
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.to_date)
                                return moment(full.to_date).format('DD-MM-YYYY');
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.reason)
                               return full.reason;
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.reply_description)
                               return full.reply_description;
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                            if(full.status == 2)
                                return 'Pending';
                            else if(full.status == 1)
                                return 'Approved';
                            else if(full.status == 0)
                                return 'Rejected';
                            else
                                return 'NIL';
                        }
                    },
                ],
            });
        }
    });
	</script>
@endsection
