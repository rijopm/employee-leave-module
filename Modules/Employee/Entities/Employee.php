<?php

namespace Modules\Employee\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Authenticatable
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
      'name','email','password','is_active'
    ];

    protected $hidden = [
         'created_at', 'updated_at','deleted_at','password','remember_token'
    ];

    protected static function newFactory()
    {
        return \Modules\Employee\Database\factories\EmployeeFactory::new();
    }
}
