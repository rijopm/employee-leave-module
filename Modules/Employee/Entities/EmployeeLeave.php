<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeLeave extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
      'employee_id','leave_type','reason','from_date','to_date', 'is_half', 'count','reply_description','status','is_active'
    ];

    protected $hidden = [
         'created_at', 'updated_at','deleted_at','password','remember_token'
    ];

    protected static function newFactory()
    {
        return \Modules\Employee\Database\factories\EmployeeLeaveFactory::new();
    }

    public function employee()
    {
        return $this->belongsTo('Modules\Employee\Entities\Employee','employee_id');
    }
}
