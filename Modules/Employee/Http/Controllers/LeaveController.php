<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Auth;

use Modules\Employee\Entities\EmployeeLeave;

class LeaveController extends Controller
{
    public function index()
    {

      $employee = Auth::user();

      $cl_balance = 6 - EmployeeLeave::where(['employee_id' => $employee->id, 'status' => 1,'leave_type' => 'cl'])->sum('count');

      $pl_balance = 6 - EmployeeLeave::where(['employee_id' => $employee->id, 'status' => 1,'leave_type' => 'pl'])->sum('count');

      $sl_balance = 6 - EmployeeLeave::where(['employee_id' => $employee->id, 'status' => 1,'leave_type' => 'sl'])->sum('count');

        return view('employee::leave.index',compact('cl_balance','pl_balance','sl_balance'));
    }

    public function list(Request $req)
    {
        $search   = $req->search['value'];
        $sort     = $req->order;
        $column   = $sort[0]['column'];
        $order    = $sort[0]['dir'] == 'asc' ? "ASC" : "DESC";

        $employee = Auth::user();

        $leave = EmployeeLeave::where('employee_id',$employee->id)->orderBy('id', 'desc');

        if ($search != '') {

            $leave->where(function($query){
              $query->where('leave_type', 'LIKE', '%'.$search.'%')
                      ->orWhere('from_date', 'LIKE', '%'.$search.'%')
                      ->orWhere('to_date', 'LIKE', '%'.$search.'%');
            });
        }

        $total = $leave->count();

        $result['data'] = $leave->take($req->length)->skip($req->start)->get();
        $result['recordsTotal'] = $total;
        $result['recordsFiltered'] =  $total;

        echo json_encode($result);
    }

    public function apply()
    {
        return view('employee::leave.apply');
    }

    public function store(Request $req)
    {
        $rules = array(
            'leave_type' => 'required|in:cl,sl,pl,lwp',
            'from_date' => 'required|date_format:Y-m-d|after_or_equal:'.date('Y-m-d'),
            'to_date' => 'required|date_format:Y-m-d|after_or_equal:from_date',
            'reason' => 'required',
        );

        $validator = Validator::make($req->all() , $rules);

        if ($validator->fails())
        {
            $res = array(
                'errorcode' => '1',
                'message' => $validator->messages()
            );
        }
        else
        {
            $employee = Auth::user();

            $current_date = now();

            $from_date = Carbon::parse($req->from_date);
            $to_date = Carbon::parse($req->to_date);

            $days = $to_date->diffInDays($from_date);

            $days++;

            if($req->is_half)
            {
                $days = $days/2;
            }

            if($req->leave_type == 'cl' || $req->leave_type == 'pl')
            {
                $leave = EmployeeLeave::where(['employee_id' => $employee->id, 'status' => 1, 'leave_type' => $req->leave_type]);

                if($leave->sum('count') >= 6)
                {
                    $res = array(
                      'errorcode' => '2',
                      'message' => 'Leave limit exceeded.'
                    );

                    return response()->json($res);
                }

                $monthly_count = $leave->whereMonth('created_at',$current_date)->whereYear('created_at',$current_date)->sum('count');

                if($monthly_count >= 2)
                {
                    $res = array(
                      'errorcode' => '2',
                      'message' => 'Leave limit exceeded in this month.'
                    );

                    return response()->json($res);
                }

                if($days > 6)
                {
                    $res = array(
                      'errorcode' => '2',
                      'message' => "Can't apply leave more than 6 days."
                    );

                    return response()->json($res);
                }
            }

            if($req->leave_type == 'sl')
            {
                $leave = EmployeeLeave::where(['employee_id' => $employee->id, 'status' => 1, 'leave_type' => $req->leave_type]);

                if($leave->sum('count') >= 6)
                {
                    $res = array(
                      'errorcode' => '2',
                      'message' => 'Leave limit exceeded.'
                    );

                    return response()->json($res);
                }

                if($days > 6)
                {
                    $res = array(
                      'errorcode' => '2',
                      'message' => "Can't apply leave more than 6 days."
                    );

                    return response()->json($res);
                }
            }

            EmployeeLeave::create([
                'employee_id' => $employee->id,
                'leave_type' => $req->leave_type,
                'from_date' => $req->from_date,
                'to_date' => $req->to_date,
                'is_half' => $req->is_half ? 1 : 0,
                'count' => $days,
                'reason' => $req->reason
            ]);

            $res = array(
              'errorcode' => '0',
              'message' => 'Leave applied successfully.'
            );
        }

        return response()->json($res);
    }
}
