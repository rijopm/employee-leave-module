<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('hr')->group(function() {

  Route::get('/', 'LoginController@index');

  Route::get('login', 'LoginController@index');

  Route::post('login', 'LoginController@login');

  Route::group(['middleware' => 'custom:hr'], function(){

    Route::get('logout', 'LoginController@logout');

    //Leave

    Route::get('leave', 'LeaveController@index');

    Route::get('leave/list', 'LeaveController@list');

    Route::get('leave/apply', 'LeaveController@apply');

    Route::post('leave/store', 'LeaveController@store');

    Route::post('leave/status', 'LeaveController@status');

  });
});
