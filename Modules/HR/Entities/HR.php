<?php

namespace Modules\HR\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class HR extends Authenticatable
{
    use HasFactory,SoftDeletes;

    protected $table = 'h_rs';

    protected $fillable = [
      'name','email','password','is_active'
    ];

    protected $hidden = [
         'created_at', 'updated_at','deleted_at','password','remember_token'
    ];

    protected static function newFactory()
    {
        return \Modules\HR\Database\factories\HRFactory::new();
    }
}
