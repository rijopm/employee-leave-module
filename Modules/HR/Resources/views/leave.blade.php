@extends('employee::layouts.master')
@section('content')
	@include('hr::layouts.header')
	@include('hr::layouts.sidebar')
  <div class="main-content">
		<div class="row">
			<div class="col-6">
		        <div class="breadcrumb">
		            <h1>Leave</h1>
		        </div>
		    </div>
	    </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row mb-4">
            <div class="col-md-12 mb-4">
                <div class="card text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-3">Leave List</h4>
                        <div class="table-responsive">
                            <table class="display table table-striped table-bordered" id="leave_table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Leave Type</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Reason</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
		<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-1" aria-hidden="true">
				<div class="modal-dialog modal-sm">
						<div class="modal-content">
								<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle-1">Reject Leave</h5>
										<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
								</div>
								<div class="modal-body">
									<input type="hidden" id="employee_id">
									<input type="hidden" id="leave_status">
									<label>Description*</label>
									<textarea class="form-control" name="description" id="description"></textarea>
								</div>
								<div class="modal-footer">
										<button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
										<button class="btn btn-primary ml-2" type="button" id="leaveStatusSubmit">Save changes</button>
								</div>
						</div>
				</div>
		</div>
	@include('employee::layouts.footer')
@endsection

@section('script')
	<script type="text/javascript">
  $(function()
    {
        if($('#leave_table').length > 0)
        {
            $('#leave_table').DataTable({
                "bLengthChange": false,
                "bInfo" : false,
                "lengthMenu": [[25]],
                "paging": true,
                processing: true,
                serverSide: true,
                ajax: base_url+'/hr/leave/list',
                columns:
                [
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full,meta)
                        {
                            return meta.settings._iDisplayStart + meta.row + 1;
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.employee.name)
                               return full.employee.name;
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.leave_type)
                           {
                             if(full.leave_type == 'cl')
                                return 'Casual Leave';
                             else if(full.leave_type == 'cl')
                                return 'Sick Leave';
                             else if(full.leave_type == 'pl')
                                return 'Paid Leave';
                             else if(full.leave_type == 'lwp')
                                return 'Leave Without Pay';
                             else
                                return 'NIL';
                           }
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.from_date)
                               return moment(full.from_date).format('DD-MM-YYYY');
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.to_date)
                                return moment(full.to_date).format('DD-MM-YYYY');
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                           if(full.reason)
                               return full.reason;
                            else
                                return 'NIL';
                        }
                    },
                    {
                        orderable: false,
                        data: "null",
                        'text-align': 'center',
                        render : function(data,type,full)
                        {
                            if(full.status == 2)
                                return 'Pending';
                            else if(full.status == 1)
                                return 'Approved';
                            else if(full.status == 0)
                                return 'Rejected';
                            else
                                return 'NIL';
                        }
                    },
                ],
                "columnDefs":
                [
                    {
                        "targets": 7,
                        "visible": true,
                        left: '500px',
                        "render": function (data, type, full)
                        {
                            if(full.status == 2)
                            {
                                var html =  '<div class="btn-group">'+
                                                '<a href="javascript:void(0)" onclick="status('+full.id+',1)" class="btn btn-sm btn-outline-success">Approve</a>'+
                                                '<a href="javascript:void(0)" onclick="status('+full.id+',2)" class="btn btn-sm btn-outline-danger">Reject</a>'+
                                            '</div>';

                                return html;
                            }
                            else
                            {
                                return 'NIL';
                            }
                        }
                    }
                ],
            });
        }
    });

		function status(id,flag)
    {
				$('#employee_id').val(id);
				$('#leave_status').val(flag);
				if(flag == 1)
				{
						$('#description').val('');
						$('#leaveStatusSubmit').click();
				}
				else if(flag == 2)
				{
						$('.modal').modal('show');
				}
		}

		$('#leaveStatusSubmit').on('click',function(){
				$.ajax({
						url: base_url+'/hr/leave/status',
						headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
						dataType: 'json',
						data:{
								id : $('#employee_id').val(),
								status : $('#leave_status').val(),
								description : $('#description').val(),
						},
						type: 'POST',
						success: function (resp) {
								if(resp.errorcode =='0')
								{
										toastr.success(resp.message,'Success');

										location.href = base_url+'/hr/leave';
								}
								else if (resp.errorcode == '1')
								{
										var html = '';
										$.each(resp.message, function(i,val){
												html+= val+'</br>';
										});
										toastr.error(html,'Error');
								}
								else if (resp.errorcode == '2')
								{
										toastr.error(resp.message,'Error');
										location.href = base_url+'/hr/leave';
								}

						}
				});
		})
	</script>
@endsection
