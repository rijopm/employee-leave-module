<div class="app-admin-wrap layout-sidebar-large">
    <div class="main-header">
        <div class="logo">
            <a href="{{ url('hr/leave') }}" target="_blank"><h6 class="m-2 text-center">Employee Leave Module</h6></a>
        </div>
        <div class="menu-toggle">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div style="margin: auto"></div>
        <div class="header-part-right">
            <!-- Full screen toggle -->
            <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
            <!-- User avatar dropdown -->
            <div class="dropdown">
                <div class="user col align-self-end">
                    <img src="{{asset('public/assets/images/faces/1.jpg')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <div class="dropdown-header">
                            <i class="i-Lock-User mr-1"></i>{{ auth()->guard('hr')->user()->name }}
                        </div>
                        <!--a class="dropdown-item">Account settings</a>
                        <a class="dropdown-item">Billing history</a-->
                        <a class="dropdown-item" href="{{ URL('hr/logout') }}"> Logout </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
