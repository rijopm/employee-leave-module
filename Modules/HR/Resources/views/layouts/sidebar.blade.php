<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{Request::is('employee/leave*') ? 'active' : ''}}">
                <a class="nav-item-hold" href="{{url('employee/leave')}}">
                    <i class="nav-icon i-Home1"></i>
                    <span class="nav-text">Leave</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
<div class="main-content-wrap sidenav-open d-flex flex-column">
