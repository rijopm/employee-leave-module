<?php

namespace Modules\HR\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Auth;

use Modules\Employee\Entities\EmployeeLeave;

class LeaveController extends Controller
{
    public function index()
    {
        return view('hr::leave');
    }

    public function list(Request $req)
    {
        $search   = $req->search['value'];
        $sort     = $req->order;
        $column   = $sort[0]['column'];
        $order    = $sort[0]['dir'] == 'asc' ? "ASC" : "DESC";

        $leave = EmployeeLeave::with('employee')->orderBy('id', 'desc');

        if ($search != '') {

            $leave->where(function($query) use ($search){
              $query->where('leave_type', 'LIKE', '%'.$search.'%')
                    ->orWhere('from_date', 'LIKE', '%'.$search.'%')
                    ->orWhere('to_date', 'LIKE', '%'.$search.'%')
                    ->whereHas('employee',function($que) use ($search){
                        $que->where('name','LIKE','%'.$search.'%');
                    });
            });
        }

        $total = $leave->count();

        $result['data'] = $leave->take($req->length)->skip($req->start)->get();
        $result['recordsTotal'] = $total;
        $result['recordsFiltered'] =  $total;

        echo json_encode($result);
    }

    public function status(Request $req)
    {
        $rules = array(
            'id' => 'required|exists:employee_leaves,id',
            'status' => 'required|in:1,2',
            'description' => 'required_if:status,==,2',
        );

        $messages = array(
          'description.required_if' => 'Description required for rejection'
        );

        $validator = Validator::make($req->all() , $rules, $messages);

        if ($validator->fails())
        {
            $res = array(
                'errorcode' => '1',
                'message' => $validator->messages()
            );
        }
        else
        {
            $employee_leave = EmployeeLeave::find($req->id);

            $current_date = now();

            if($req->status == 1)
            {
                if($employee_leave->leave_type == 'cl' || $employee_leave->leave_type == 'pl')
                {
                    $leave = EmployeeLeave::where(['employee_id' => $employee_leave->employee_id, 'status' => 1, 'leave_type' => $employee_leave->leave_type]);

                    if($leave->sum('count') >= 6)
                    {
                        $res = array(
                          'errorcode' => '2',
                          'message' => 'Leave limit exceeded.'
                        );

                        $employee_leave->update(['status' => 0, 'reply_description' => $res['message']]);

                        return response()->json($res);
                    }

                    $monthly_count = $leave->whereMonth('created_at',$current_date)->whereYear('created_at',$current_date)->sum('count');

                    if($monthly_count >= 2)
                    {
                        $res = array(
                          'errorcode' => '2',
                          'message' => 'Leave limit exceeded in this month.'
                        );

                        $employee_leave->update(['status' => 0, 'reply_description' => $res['message']]);

                        return response()->json($res);
                    }

                    if($employee_leave->count > 6)
                    {
                        $res = array(
                          'errorcode' => '2',
                          'message' => "Can't apply leave more than 6 days."
                        );

                        $employee_leave->update(['status' => 0, 'reply_description' => $res['message']]);

                        return response()->json($res);
                    }
                }

                if($req->leave_type == 'sl')
                {
                    $leave = EmployeeLeave::where(['employee_id' => $employee_leave->employee_id, 'status' => 1, 'leave_type' => $employee_leave->leave_type]);

                    if($leave->sum('count') >= 6)
                    {
                        $res = array(
                          'errorcode' => '2',
                          'message' => 'Leave limit exceeded.'
                        );

                        $employee_leave->update(['status' => 0, 'reply_description' => $res['message']]);

                        return response()->json($res);
                    }

                    if($employee_leave->count > 6)
                    {
                        $res = array(
                          'errorcode' => '2',
                          'message' => "Can't apply leave more than 6 days."
                        );

                        $employee_leave->update(['status' => 0, 'reply_description' => $res['message']]);

                        return response()->json($res);
                    }
                }

                $employee_leave->update(['status' => 1]);

                $res = array(
                  'errorcode' => '0',
                  'message' => 'Leave approved successfully.'
                );
            }
            else
            {
                $employee_leave->update(['status' => 0, 'reply_description' => $req->description]);

                $res = array(
                  'errorcode' => '0',
                  'message' => 'Leave rejected successfully.'
                );
            }
        }

        return response()->json($res);
    }
}
