<?php

namespace Modules\HR\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;

use Modules\HR\Entities\HR;



class LoginController extends Controller
{
    public function index()
    {
        return view('hr::login');
    }

    function login(Request $req)
    {
        $rules = array(
            'email'  => 'required|exists:h_rs,email',
            'password'  => 'required',
        );

        $messages = array(
            'email.exists' => 'Enter valid credentials and try again.'
        );

        $validator = Validator::make($req->all() , $rules, $messages);

        if ($validator->fails())
        {
            $res = array(
                'errorcode' => '1',
                'message' => $validator->messages()
            );
        }
        else
        {
            $user = HR::where('email',$req->email)->first();

            if($user == null)
            {
                $res = array(
                  'errorcode' => '2',
                  'message' => 'No such user exist.'
                );
            }
            else
            {
                $credentials = $req->only('email', 'password');

                // dd($credentials);

                if (Auth::guard('hr')->attempt($credentials, $req->has('remember')))
                {
                    $user = Auth::guard('hr')->user();

                    $res = array(
                      'errorcode' => '0',
                      'message' => 'Login successfull.'
                    );

                }
                else
                {
                    $res = array(
                      'errorcode' => '2',
                      'message' => 'Enter valid credentials and try again.'
                    );
                }

            }
        }

        return response()->json($res);
    }

    public function logout()
    {
        Auth::guard('hr')->logout();

        return redirect('hr');
    }
}
