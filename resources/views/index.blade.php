@extends('employee::layouts.master')
@section('content')
<div class="auth-layout-wrap" style="background-image: url({{asset('public/assets/images/photo-wide-4.jpg')}})">
    <div class="auth-content">
      <section class="widget-card">
          <div class="row">
              <div class="col-lg-6 mt-3">
                  <div class="card">
                      <div class="card-body">
                          <h5 class="card-title mb-2">Employee</h5>
                          <a class="btn btn-outline-success ul-btn-raised--v2 m-1 float-right" href="{{url('employee')}}">Login</a>
                      </div>
                  </div>
              </div>
              <div class="col-lg-6 mt-3">
                  <div class="card">
                      <div class="card-body">
                          <h5 class="card-title mb-2">HR</h5>
                          <a class="btn btn-outline-success ul-btn-raised--v2 m-1 float-right" href="{{url('hr')}}">Login</a>
                      </div>
                  </div>
              </div>
          </div>
              </section>
    </div>
</div>
@endsection
